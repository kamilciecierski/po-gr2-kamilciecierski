package pl.edu.uwm.wmii.kamilciecierski.kolokwium;

import java.util.Scanner;

public class Zad2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m;
        int n;

        System.out.print("Podaj dodatnią liczbę m<5: ");
        while(true) {
            m = scanner.nextInt();
            if(m > 0 && m < 5){
                break;
            }
            else{
                System.out.println("Podana liczba jest nieprawiłowa!");
            }
        }

        System.out.print("Podaj dodatnią liczbę n<5: ");
        while(true) {
            n = scanner.nextInt();
            if(n > 0 && n < 5){
                break;
            }
            else{
                System.out.println("Podana liczba jest nieprawidłowa!");
            }
        }

        int[][] tab = wczytajElementy();
        wypisz(tab,4);

        int[][] tabB = macierz(tab,m,n);
        wypisz(tabB,3);

    }

    public static int[][] wczytajElementy(){
        int[][] a = new int[4][4];
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj elementy tablicy: ");
        for(int i=0; i<4; i++){
            for(int j=0; j<4; j++){
                a[i][j] = scanner.nextInt();
            }
        }
        return a;
    }

    public static int[][] macierz(int[][] tab, int m, int n){
        int[][] b = new int[3][3];
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                if(i==m){
                    b[i][j] = tab[i+1][j];
                }
                else{
                    b[i][j] = tab[i][j];
                }
                if(j==n){
                    b[i][j] = tab[i][j+1];
                }
                else{
                    b[i][j] = tab[i][j];
                }
            }
        }
        return b;
    }


    public static void wypisz(int[][] tab, int n){
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(tab[i][j] + " ");
            }
            System.out.println();
        }
    }
}
