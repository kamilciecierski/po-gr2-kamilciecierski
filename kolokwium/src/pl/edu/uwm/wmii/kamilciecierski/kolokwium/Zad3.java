package pl.edu.uwm.wmii.kamilciecierski.kolokwium;

import java.util.Scanner;

public class Zad3 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ile liczb chcesz wpisać?: ");
        int n = scanner.nextInt();

        float[] tab = new float[n];
        for(int i=0; i<n; i++){
            System.out.print("Wpisz liczbę: ");
            tab[i] = scanner.nextFloat();
        }

        fun(tab,n);
    }

    public static void fun(float[] tab, int n){
        int wieksze = 0;
        int mniejsze = 0;
        int rowne = 0;
        for(int i=0; i<n; i++){
            if(tab[i]>7){
                wieksze++;
            }
            else if(tab[i] < -7){
                mniejsze++;
            }
            else if(tab[i] == -7){
                rowne++;
            }
        }
        System.out.println("Ilość liczb większych od 7: " + wieksze);
        System.out.println("Ilość liczb mniejszych od -7: " + mniejsze);
        System.out.println("Ilość liczb równych -7: " + rowne);
    }
}