package pl.edu.uwm.wmii.kamilciecierski.kolokwium;

import java.util.Random;
import java.util.Scanner;

public class Zad4 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj liczbę od 1 do 200: ");
        int n;
        while(true){
            n = scanner.nextInt();
            if(n >= 1 && n <= 200){
                break;
            }
            else{
                System.out.println("Liczba musi być z przedziału <1,200>");
            }
        }
        fun(n);
    }

    public static void fun(int n){
        Random r = new Random();
        int[] tab = new int[n];
        for(int i=0; i<n; i++){
            tab[i] = r.nextInt(4445)-2222;
        }

        System.out.println("Elementy tablicy: ");
        for(int i=0; i<tab.length; i++){
            System.out.print(tab[i] + ", ");
        }
        System.out.println();

        int najmniejszy = tab[0];
        int ile=0;
        for(int i=1; i<tab.length; i++){
            if(tab[i]<najmniejszy){
                najmniejszy = tab[i];
            }
        }
        for(int i=0; i<tab.length; i++){
            if(tab[i]==najmniejszy){
                ile++;
            }
        }
        System.out.println("Najmniejszy element: " + najmniejszy + " , wystąpił " + ile + " razy");
        System.out.println();
    }
}
