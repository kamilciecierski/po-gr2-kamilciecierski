package pl.imiajd.ciecierski;

public class Autobus extends Transport{
    public Autobus(int iloscMiejsc) {
        this.iloscMiejsc = iloscMiejsc;
        super.obliczCene();
    }

    @Override
    public String toString() {
        return "Autobus: " +
                "ilość miejsc: " + iloscMiejsc +
                ", cena biletu: " + cenaBiletu;
    }
}
