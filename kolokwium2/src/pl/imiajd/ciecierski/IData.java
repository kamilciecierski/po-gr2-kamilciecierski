package pl.imiajd.ciecierski;

import java.time.LocalDateTime;

public interface IData {
    void UstawDate(LocalDateTime data);
    boolean SprawdzDate();
}
