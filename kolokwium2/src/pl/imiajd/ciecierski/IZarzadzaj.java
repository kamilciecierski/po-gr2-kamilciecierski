package pl.imiajd.ciecierski;

public interface IZarzadzaj {
    void DodajAutobus(int iloscMiejsc);
    void DodajPociag(int iloscMiejsc, int dlugoscTrasy);
    void UsunOstatni();
    void Wyczysc();
}
