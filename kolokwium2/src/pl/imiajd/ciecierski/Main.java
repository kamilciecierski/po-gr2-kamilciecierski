package pl.imiajd.ciecierski;

import java.time.LocalDateTime;

public class Main {

    public static void main(String[] args) {
        Autobus bus1 = new Autobus(50);
        Pociag train1 = new Pociag(50,200);
        Podroz trip1 = new Podroz(LocalDateTime.of(2020,12,3,19,20));

        System.out.println(bus1);

        train1.obliczCene();
        System.out.println(train1);


        System.out.println(trip1);
        trip1.DodajAutobus(20);
        System.out.println(trip1);
        trip1.DodajPociag(240,200);
        System.out.println(trip1);
        trip1.UsunOstatni();
        System.out.println(trip1);
        System.out.println(trip1.SprawdzDate());
        trip1.UstawDate(LocalDateTime.of(2020,12,20,14,20));
        System.out.println(trip1.SprawdzDate());
        trip1.Wyczysc();
        System.out.println(trip1);
    }
}
