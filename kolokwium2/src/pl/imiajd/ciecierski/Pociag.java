package pl.imiajd.ciecierski;

public class Pociag extends Transport{
    public Pociag(int iloscMiejsc, int dlugoscTrasy) {
        this.iloscMiejsc = iloscMiejsc;
        this.dlugoscTrasy = dlugoscTrasy;
        this.obliczCene();
    }


    @Override
    void obliczCene() {
        if(this.dlugoscTrasy > 100){
            super.cenaBiletu = dlugoscTrasy*1.45;
        }
        else{
            super.obliczCene();
        }
    }

    @Override
    public String toString() {
        return "Pociag: " +
                "ilość miejsc: " + iloscMiejsc +
                ", długość trasy: " + dlugoscTrasy +
                ", cena biletu: " + cenaBiletu;
    }

    private int dlugoscTrasy;
}
