package pl.imiajd.ciecierski;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Podroz implements IZarzadzaj, IData{
    public Podroz(LocalDateTime dataPodrozy) {
        this.dataPodrozy = dataPodrozy;
        this.planPodrozy = new ArrayList<>();
    }


    @Override
    public void UstawDate(LocalDateTime data) {
        this.dataPodrozy = data;
    }

    @Override
    public boolean SprawdzDate() {
        if(this.dataPodrozy.isAfter(LocalDateTime.now())){
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public void DodajAutobus(int iloscMiejsc) {
        this.planPodrozy.add(new Autobus(iloscMiejsc));
        this.koszt += new Autobus(iloscMiejsc).getCenaBiletu();
    }

    @Override
    public void DodajPociag(int iloscMiejsc, int dlugoscTrasy) {
        this.planPodrozy.add(new Pociag(iloscMiejsc,dlugoscTrasy));
        this.koszt += new Pociag(iloscMiejsc,dlugoscTrasy).getCenaBiletu();
    }

    @Override
    public void UsunOstatni() {
        this.koszt -= planPodrozy.get(planPodrozy.size()-1).getCenaBiletu();
        this.planPodrozy.remove(planPodrozy.size()-1);
    }

    @Override
    public void Wyczysc() {
        Iterator<Transport> planPodrozyIterator = planPodrozy.iterator();
        while(planPodrozyIterator.hasNext()){
            planPodrozyIterator.next();
            planPodrozyIterator.remove();
        }

        this.koszt = 0;
    }


    @Override
    public String toString() {
        return "Podroz: " +
                "dataPodrozy: " + dataPodrozy +
                ", planPodrozy: " + planPodrozy +
                ", koszt: " + koszt;
    }

    private LocalDateTime dataPodrozy;
    private List<Transport> planPodrozy;
    private double koszt = 0;
}
