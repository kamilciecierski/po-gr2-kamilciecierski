package pl.imiajd.ciecierski;

abstract class Transport {
    public Transport() {}

    void obliczCene(){
        this.cenaBiletu = 110;
    }

    public double getCenaBiletu() {
        return cenaBiletu;
    }

    public int iloscMiejsc;
    protected double cenaBiletu;
}
