package pl.edu.uwm.wmii.kamilciecierski.laboratorium00;

public class Zadanie11 {

    public static void main(String[] args){
        System.out.println("\"Elegia o chłopcu polskim\" - Krzysztof Kamil Baczyński\n");
        System.out.print("Oddzielili cię, syneczku, od snów, co jak motyl drżą,\n" +
                "haftowali ci, syneczku, smutne oczy rudą krwią,\n" +
                "malowali krajobrazy w żółe ściegi pożóg,\n" +
                "wyszywali wisielcami drzew płynące morze.\n" +
                "\n" +
                "Wyuczyli cię, syneczku, ziemi twej na pamięć,\n" +
                "gdyś jej ścieżki powycinał żelaznymi łzami.\n" +
                "Odchowali cię w ciemności, odkarmili bochnem trwóg,\n" +
                "przemierzyłeś po omacku najwstydliwsze z ludzkich dróg.\n" +
                "\n" +
                "I wyszedłeś, jasny synku, z czarną bronią w noc,\n" +
                "i poczułeś, jak się jeży w dźwięku minut - zło.\n" +
                "Zanim padłeś, jeszcze ziemię przeżegnałeś ręką.\n" +
                "Czy to była kula, synku, czy to serce pękło?\n");
    }
}
