package pl.edu.uwm.wmii.kamilciecierski.laboratorium00;

public class Zadanie2 {

    public static void main(String[] args){
        System.out.println(sum());
    }

    public static int sum(){
        int suma = 0;
        for(int i=1; i<=10; i++){
            suma+=i;
        }
        return suma;
    }
}