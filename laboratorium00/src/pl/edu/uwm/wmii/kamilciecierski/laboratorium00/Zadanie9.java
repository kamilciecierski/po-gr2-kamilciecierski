package pl.edu.uwm.wmii.kamilciecierski.laboratorium00;

public class Zadanie9 {
    public static void main(String[] args){
        System.out.println(" .--.              .--.");
        System.out.println(": (\\ \". _......_ .\" /) :");
        System.out.println(" '.    `        `    .'");
        System.out.println("  /'   _        _   `\\          -----");
        System.out.println(" /     0}      {0     \\       / Hello \\");
        System.out.println("|       /      \\       |     <  Junior |");
        System.out.println("|     /'        `\\     |      \\ Coder!/");
        System.out.println(" \\   | .  .==.  . |   /         -----");
        System.out.println("  '._ \\.' \\__/ './ _.'");
        System.out.println("  /  ``'._-''-_.'``  \\");
        System.out.println("          `--`s");

    }
}
