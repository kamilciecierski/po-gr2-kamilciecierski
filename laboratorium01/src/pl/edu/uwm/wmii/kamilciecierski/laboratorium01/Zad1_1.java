package pl.edu.uwm.wmii.kamilciecierski.laboratorium01;
import java.util.Scanner;
import static java.lang.Math.*;

public class Zad1_1 {

    public static void main(String[] args) {
        //System.out.println("Wynik: " + pktA());
        //System.out.println("Wynik: " + pktB());
        //System.out.println("Wynik: " + pktC());
        //System.out.println("Wynik: " + pktD());
        //System.out.println("Wynik: " + pktE());
        //System.out.println("Wynik: " + pktF());
        //pktG();
        //System.out.println("Wynik: " + pktH());
        System.out.println("Wynik: " + pktI());
    }

    public static float pktA(){
        Scanner scanner = new Scanner(System.in);
        int n;
        float wynik = 0;
        System.out.print("Ile liczb chcesz podać?: ");
        n = scanner.nextInt();
        for(int i=0; i<n; i++){
            System.out.print("Podaj liczbę: ");
            wynik+=scanner.nextFloat();
        }
        return wynik;
    }

    public static float pktB(){
        Scanner scanner = new Scanner(System.in);
        int n;
        float wynik = 1;
        System.out.print("Ile liczb chcesz podać?: ");
        n = scanner.nextInt();
        for(int i=0; i<n; i++){
            System.out.print("Podaj liczbę: ");
            wynik*=scanner.nextFloat();
        }
        return wynik;
    }

    public static float pktC(){
        Scanner scanner = new Scanner(System.in);
        int n;
        float wynik = 0;
        System.out.print("Ile liczb chcesz podać?: ");
        n = scanner.nextInt();
        for(int i=0; i<n; i++){
            System.out.print("Podaj liczbę: ");
            wynik+=abs(scanner.nextFloat());
        }
        return wynik;
    }

    public static float pktD(){
        Scanner scanner = new Scanner(System.in);
        int n;
        float wynik = 0;
        System.out.print("Ile liczb chcesz podać?: ");
        n = scanner.nextInt();
        for(int i=0; i<n; i++){
            System.out.print("Podaj liczbę: ");
            wynik+=sqrt(abs(scanner.nextFloat()));
        }
        return wynik;
    }

    public static float pktE(){
        Scanner scanner = new Scanner(System.in);
        int n;
        float wynik = 1;
        System.out.print("Ile liczb chcesz podać?: ");
        n = scanner.nextInt();
        for(int i=0; i<n; i++){
            System.out.print("Podaj liczbę: ");
            wynik*=abs(scanner.nextFloat());
        }
        return wynik;
    }

    public static float pktF(){
        Scanner scanner = new Scanner(System.in);
        int n;
        float wynik = 0;
        System.out.print("Ile liczb chcesz podać?: ");
        n = scanner.nextInt();
        for(int i=0; i<n; i++){
            System.out.print("Podaj liczbę: ");
            wynik+=pow(scanner.nextFloat(),2);
        }
        return wynik;
    }

    public static void pktG(){
        Scanner scanner = new Scanner(System.in);
        int n;
        float wynik1 = 0;
        float wynik2 = 1;
        System.out.print("Ile liczb chcesz podać?: ");
        n = scanner.nextInt();
        for(int i=0; i<n; i++){
            System.out.print("Podaj liczbę: ");
            float pom = scanner.nextFloat();
            wynik1+=pom;
            wynik2*=pom;
        }
        System.out.println("Wynik sumy: " + wynik1);
        System.out.println("Wynik mnożenia: " + wynik2);
    }

    public static float pktH(){
        Scanner scanner = new Scanner(System.in);
        int n;
        float wynik = 0;
        System.out.print("Ile liczb chcesz podać?: ");
        n = scanner.nextInt();
        for(int i=1; i<=n; i++){
            System.out.print("Podaj liczbę: ");
            float an = scanner.nextFloat();
            wynik+=pow(-1,i+1)*an;
        }
        return wynik;
    }

    public static float pktI(){
        Scanner scanner = new Scanner(System.in);
        int n;
        int sil = 1;
        float wynik = 0;
        System.out.print("Ile liczb chcesz podać?: ");
        n = scanner.nextInt();
        for(int i=1; i<=n; i++){
            System.out.print("Podaj liczbę: ");
            sil*=i;
            float an = scanner.nextFloat();
            wynik+=pow(-1,i)*an/sil;
        }
        return wynik;
    }
}
