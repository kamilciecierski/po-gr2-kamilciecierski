package pl.edu.uwm.wmii.kamilciecierski.laboratorium01;
import java.util.Scanner;

public class Zad1_2 {
    public static void main(String[] args){
        fun();
    }

    public static void fun(){
        Scanner scanner = new Scanner(System.in);
        int n;
        System.out.print("Ile liczb chcesz podać?: ");
        n = scanner.nextInt();
        float[] tab = new float[n];
        for(int i=0; i<n; i++){
            System.out.print("Podaj liczbę: ");
            tab[i] = scanner.nextFloat();
        }
        for(int j=1; j<n; j++){
            System.out.print(tab[j] + ", ");
        }
        System.out.println(tab[0]);
    }
}
