package pl.edu.uwm.wmii.kamilciecierski.laboratorium01;
import java.util.Scanner;
import static java.lang.Math.*;

public class Zad2_1 {
    public static void main(String[] args){
        //System.out.println(pktA());
        //System.out.println(pktB());
        //System.out.println(pktC());
        //System.out.println(pktD());
        //System.out.println(pktE());
        //System.out.println(pktF());
        //System.out.println(pktG());
        System.out.println(pktH());

    }

    public static int pktA(){
        Scanner scanner = new Scanner(System.in);
        int n;
        int ilosc = 0;
        int tmp;
        System.out.print("Ile liczb chcesz podać?: ");
        n = scanner.nextInt();
        for(int i=0; i<n; i++){
            System.out.print("Podaj liczbę: ");
            tmp = scanner.nextInt();
            if(tmp%2==1){
                ilosc++;
            }
        }
        return ilosc;
    }

    public static int pktB(){
        Scanner scanner = new Scanner(System.in);
        int n;
        int ilosc = 0;
        int tmp;
        System.out.print("Ile liczb chcesz podać?: ");
        n = scanner.nextInt();
        for(int i=0; i<n; i++){
            System.out.print("Podaj liczbę: ");
            tmp = scanner.nextInt();
            if(tmp%3==0 && tmp%5!=0){
                ilosc++;
            }
        }
        return ilosc;
    }

    public static int pktC(){
        Scanner scanner = new Scanner(System.in);
        int n;
        int ilosc = 0;
        int tmp;
        System.out.print("Ile liczb chcesz podać?: ");
        n = scanner.nextInt();
        for(int i=0; i<n; i++){
            System.out.print("Podaj liczbę: ");
            tmp = scanner.nextInt();
            if(sqrt(tmp)%2==0){
                ilosc++;
            }
        }
        return ilosc;
    }

    public static int pktD(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ile liczb chcesz podać?: ");
        int n;
        n = scanner.nextInt();
        int[] tab = new int[n];
        int ilosc = 0;
        for(int i=0; i<n; i++){
            System.out.print("Podaj liczbę: ");
            tab[i] = scanner.nextInt();
        }
        for(int i=1; i<n-1; i++){
            if(tab[i]<(tab[i-1]+tab[i+1])/2){
                ilosc++;
            }
        }
        return ilosc;
    }

    public static int pktE(){
        Scanner scanner = new Scanner(System.in);
        int n;
        int ilosc = 0;
        int tmp;
        int silnia = 1;
        System.out.print("Ile liczb chcesz podać?: ");
        n = scanner.nextInt();
        for(int i=1; i<=n; i++){
            System.out.print("Podaj liczbę: ");
            tmp = scanner.nextInt();
            silnia *= i;
            if(tmp>pow(2,i) && tmp<silnia){
                ilosc++;
            }
        }
        return ilosc;
    }

    public static int pktF(){
        Scanner scanner = new Scanner(System.in);
        int n;
        int ilosc = 0;
        int tmp;
        System.out.print("Ile liczb chcesz podać?: ");
        n = scanner.nextInt();
        for(int i=1; i<=n; i++){
            System.out.print("Podaj liczbę: ");
            tmp = scanner.nextInt();
            if(i%2==1 && tmp%2==0){
                ilosc++;
            }
        }
        return ilosc;
    }

    public static int pktG(){
        Scanner scanner = new Scanner(System.in);
        int n;
        int ilosc = 0;
        int tmp;
        System.out.print("Ile liczb chcesz podać?: ");
        n = scanner.nextInt();
        for(int i=0; i<n; i++){
            System.out.print("Podaj liczbę: ");
            tmp = scanner.nextInt();
            if(tmp%2==1 && tmp>=0){
                ilosc++;
            }
        }
        return ilosc;
    }

    public static int pktH(){
        Scanner scanner = new Scanner(System.in);
        int n;
        int ilosc = 0;
        int tmp;
        System.out.print("Ile liczb chcesz podać?: ");
        n = scanner.nextInt();
        for(int i=1; i<=n; i++){
            System.out.print("Podaj liczbę: ");
            tmp = scanner.nextInt();
            if(abs(tmp)<pow(i,2)){
                ilosc++;
            }
        }
        return ilosc;
    }
}
