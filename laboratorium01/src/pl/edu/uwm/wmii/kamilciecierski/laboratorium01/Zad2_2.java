package pl.edu.uwm.wmii.kamilciecierski.laboratorium01;
import java.util.Scanner;

public class Zad2_2 {
    public static void main(String[] args){
        System.out.println(podwojona_suma());
    }

    public static float podwojona_suma(){
        Scanner scanner = new Scanner(System.in);
        int n;
        float tmp;
        float wynik = 0;
        System.out.print("Ile liczb chcesz podać?: ");
        n = scanner.nextInt();
        for(int i=0; i<n; i++){
            System.out.print("Podaj liczbę: ");
            tmp = scanner.nextFloat();
            if(tmp>0){
                wynik+=tmp;
            }
        }
        return 2*wynik;
    }

}
