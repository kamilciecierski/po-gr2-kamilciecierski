package pl.edu.uwm.wmii.kamilciecierski.laboratorium01;
import java.util.Scanner;

public class Zad2_3 {
    public static void main(String[] args){
        fun();
    }

    public static void fun(){
        Scanner scanner = new Scanner(System.in);
        int n;
        float tmp;
        int dodatnie = 0;
        int ujemene = 0;
        int zera = 0;
        System.out.print("Ile liczb chcesz podać?: ");
        n = scanner.nextInt();
        for(int i=0; i<n; i++){
            System.out.print("Podaj liczbę: ");
            tmp = scanner.nextFloat();
            if(tmp>0){
                dodatnie++;
            }
            else if(tmp<0){
                ujemene++;
            }
            else{
                zera++;
            }
        }
        System.out.println("Ilość liczb dodatnich: " + dodatnie);
        System.out.println("Ilość liczb ujemnych: " + ujemene);
        System.out.println("Ilość zer: " + zera);
    }
}
