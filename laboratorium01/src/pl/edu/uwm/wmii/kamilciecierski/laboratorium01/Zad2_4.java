package pl.edu.uwm.wmii.kamilciecierski.laboratorium01;
import java.util.Scanner;

public class Zad2_4 {
    public static void main(String[] args) {
        fun();
    }

    public static void fun(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ile liczb chcesz podać?: ");
        int n;
        n = scanner.nextInt();
        System.out.print("Podaj liczbę: ");
        float tmp1 = scanner.nextFloat();
        float max = tmp1;
        float min = tmp1;
        for(int i=1; i<n; i++){
            System.out.print("Podaj liczbę: ");
            float tmp2 = scanner.nextFloat();
            if(tmp2>max){
                max = tmp2;
            }
            if(tmp2<min){
                min = tmp2;
            }
        }
        System.out.println("Największa liczba: " + max);
        System.out.println("Najmniejsza liczba: " + min);
    }
}