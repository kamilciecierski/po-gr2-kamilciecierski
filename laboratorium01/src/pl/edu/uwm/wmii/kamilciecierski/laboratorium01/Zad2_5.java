package pl.edu.uwm.wmii.kamilciecierski.laboratorium01;
import java.util.Scanner;

public class Zad2_5 {
    public static void main(String[] args) {
        System.out.println(fun());
    }

    public static int fun(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ile liczb chcesz podać?: ");
        int n;
        n = scanner.nextInt();
        float[] tab = new float[n];
        int ilosc = 0;
        for(int i=0; i<n; i++){
            System.out.print("Podaj liczbę: ");
            tab[i] = scanner.nextFloat();
        }
        for(int j=0; j<n-1; j++){
            if(tab[j]>0 && tab[j+1]>0){
                ilosc++;
            }
        }
        return ilosc;
    }
}