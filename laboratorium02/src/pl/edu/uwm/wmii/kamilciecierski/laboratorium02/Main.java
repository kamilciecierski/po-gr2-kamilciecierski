package pl.edu.uwm.wmii.kamilciecierski.laboratorium02;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

//        //zad1
//
//        System.out.print("Podaj liczbę od 1 do 100: ");
//        int n;
//
//        while (true) {
//            n = scanner.nextInt();
//            if (n >= 1 && n <= 100) {
//                break;
//            } else {
//                System.out.println("Liczba musi być z przedziału <1,100>");
//            }
//        }
//        System.out.println();
//
//        int[] tab = new int[n];
//
//        Zad1 z1 = new Zad1();
//        z1.z(tab, n);
//
//
//
//        //zad2
//
//        Zad2 z2 = new Zad2();
//        z2.generuj(tab, n, -999, 999);
//        System.out.println();
//        System.out.println(z2.ileNieparzystych(tab));
//        System.out.println(z2.ileParzystych(tab));
//        System.out.println(z2.ileDodatnich(tab));
//        System.out.println(z2.ileUjemnych(tab));
//        System.out.println(z2.ileZerowych(tab));
//        System.out.println(z2.ileMaksymalnych(tab));
//        System.out.println(z2.sumaDodatnich(tab));
//        System.out.println(z2.sumaUjemnych(tab));
//        System.out.println(z2.dlugoscMaksymalnegoCiaguDodatnich(tab));
//        z2.odwrocFragment(tab, 2, 4);
//        z2.signum(tab);
//

        //zad 3

        int m;
        int n2;
        int k;

        System.out.print("Podaj liczbę od 1 do 10 (m): ");
        while (true) {
            m = scanner.nextInt();
            if (m >= 1 && m <= 10) {
                break;
            } else {
                System.out.println("Liczba musi być z przedziału [1,10]");
            }
        }

        System.out.print("Podaj liczbę od 1 do 10 (n): ");
        while (true) {
            n2 = scanner.nextInt();
            if (n2 >= 1 && n2 <= 10) {
                break;
            } else {
                System.out.println("Liczba musi być z przedziału [1,10]");
            }
        }

        System.out.print("Podaj liczbę od 1 do 10 (k): ");
        while (true) {
            k = scanner.nextInt();
            if (k >= 1 && k <= 10) {
                break;
            } else {
                System.out.println("Liczba musi być z przedziału [1,10]");
            }
        }
        System.out.println();


        Zad3 z3 = new Zad3();
        z3.fun(m,n2,k);
    }
}