package pl.edu.uwm.wmii.kamilciecierski.laboratorium02;
import java.util.Random;
import java.util.Scanner;

public class Zad1 {
    public static void z(int tab[], int n){
        Random r = new Random();

        for(int i=0; i<n; i++){
            tab[i] = r.nextInt(1999)-999;
        }

        System.out.println("Elementy tablicy: ");
        for(int i=0; i<tab.length; i++){
            System.out.println(tab[i]);
        }
        System.out.println();


        //pkt a
        int nieparzyste = 0;
        int parzyste = 0;
        for(int i=0; i<tab.length; i++) {
            if(tab[i] % 2 == 0){
                parzyste++;
            }
            else{
                nieparzyste++;
            }
        }
        System.out.println("Ilość liczb nieparzystych: " + nieparzyste + "\nIlość liczb parzystych: " + parzyste);
        System.out.println();


        //pkt b
        int ujemne = 0;
        int dodatnie = 0;
        int zera = 0;
        for(int i=0; i<tab.length; i++) {
            if (tab[i] > 0) {
                dodatnie++;
            } else if (tab[i] < 0) {
                ujemne++;
            } else {
                zera++;
            }
        }
        System.out.println("Ilość liczb dodatnich: " + dodatnie);
        System.out.println("Ilość liczb ujemnych: " + ujemne);
        System.out.println("Ilość zer: " + zera);
        System.out.println();


        //pkt c
        int najwiekszy = tab[0];
        int ile=0;
        for(int i=1; i<tab.length; i++){
            if(tab[i]>najwiekszy){
                najwiekszy = tab[i];
            }
        }
        for(int i=0; i<tab.length; i++){
            if(tab[i]==najwiekszy){
                ile++;
            }
        }
        System.out.println("Największy element: " + najwiekszy + " , wystąpił " + ile + " razy");
        System.out.println();


        //pkt d
        int suma_ujemne = 0;
        int suma_dodatnie = 0;
        for(int i=0; i<tab.length; i++) {
            if (tab[i] < 0) {
                suma_ujemne += tab[i];
            }
            else{
                suma_dodatnie += tab[i];
            }
        }
        System.out.println("Suma ujemnych elementów tablicy: " + suma_ujemne);
        System.out.println("Suma dodatnich elementów tablicy: " + suma_dodatnie);
        System.out.println();


        //pkt e
        int naj_dlugosc = 0;
        int ilosc = 0;
        for(int i=0; i<tab.length; i++){
            if(tab[i]>naj_dlugosc){
                ilosc++;
            }
            else{
                ilosc = 0;
            }
            if(ilosc>naj_dlugosc){
                naj_dlugosc = ilosc;
            }
        }
        System.out.println("Długość najdłuższego fragmentu dodatniegu tablicy: " + naj_dlugosc);
        System.out.println();


        //pkt g
        int lewy;
        int prawy;
        int tmp;
        System.out.println("Podaj wartość \"lewy\" z przedziału <1," + n +")");
        Scanner scanner = new Scanner(System.in);

        while(true){
            lewy = scanner.nextInt();
            if(lewy>=1 && lewy<n){
                break;
            }
            else{
                System.out.println("Liczba musi być z przedziału <1," + n +")");
            }
        }

        System.out.println("Podaj wartość \"prawy\" z przedziału <1," + n +")");
        while(true){
            prawy = scanner.nextInt();
            if(prawy>=1 && prawy<n){
                break;
            }
            else{
                System.out.println("Liczba musi być z przedziału <1," + n +")");
            }
        }

        for(int i=0; i<(prawy - lewy) / 2; i++){
            tmp = tab[lewy+i];
            tab[lewy+i] = tab[prawy - i];
            tab[prawy - i] = tmp;
        }

        for(int i=0; i<tab.length; i++){
            System.out.print(tab[i] + ", ");
        }
        System.out.println();



        //pkt f
        for(int i=0; i<tab.length; i++){
            if(tab[i]>0){
                tab[i]=1;
            }
            else{
                tab[i]=-1;
            }
            System.out.print(tab[i] + ", ");
        }
        System.out.println("\n");
    }
}