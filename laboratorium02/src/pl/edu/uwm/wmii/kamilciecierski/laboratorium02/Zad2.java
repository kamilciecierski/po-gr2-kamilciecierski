package pl.edu.uwm.wmii.kamilciecierski.laboratorium02;
import java.util.Random;

public class Zad2 {
    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc){
        Random r = new Random();
        for (int i = 0; i < n; i++) {
            tab[i] = r.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
            System.out.println(tab[i]);
        }
    }

    public static int ileNieparzystych(int tab[]){
        int nieparzyste = 0;
        for(int i=0; i<tab.length; i++) {
            if(tab[i] % 2 != 0){
                nieparzyste++;
            }
        }
        return nieparzyste;
    }

    public static int ileParzystych(int tab[]){
        int parzyste = 0;
        for(int i=0; i<tab.length; i++) {
            if(tab[i] % 2 == 0){
                parzyste++;
            }
        }
        return parzyste;
    }

    public static int ileDodatnich(int tab[]){
        int dodatnie = 0;
        for(int i=0; i<tab.length; i++) {
            if (tab[i] > 0) {
                dodatnie++;
            }
        }
        return dodatnie;
    }

    public static int ileUjemnych(int tab[]){
        int ujemne = 0;
        for(int i=0; i<tab.length; i++) {
            if (tab[i] < 0) {
                ujemne++;
            }
        }
        return ujemne;
    }

    public static int ileZerowych(int tab[]){
        int zera = 0;
        for(int i=0; i<tab.length; i++) {
            if (tab[i] == 0) {
                zera++;
            }
        }
        return zera;
    }

    public static int ileMaksymalnych(int tab[]){
        int najwiekszy = tab[0];
        int ile=0;
        for(int i=1; i<tab.length; i++){
            if(tab[i]>najwiekszy){
                najwiekszy = tab[i];
            }
        }
        for(int i=0; i<tab.length; i++){
            if(tab[i]==najwiekszy){
                ile++;
            }
        }
        return ile;
    }

    public static int sumaDodatnich(int tab[]){
        int suma_dodatnie = 0;
        for(int i=0; i<tab.length; i++) {
            if (tab[i] > 0) {
                suma_dodatnie += tab[i];
            }
        }
        return suma_dodatnie;
    }

    public static int sumaUjemnych(int tab[]){
        int suma_ujemne = 0;
        for(int i=0; i<tab.length; i++) {
            if (tab[i] < 0) {
                suma_ujemne += tab[i];
            }
        }
        return suma_ujemne;
    }

    public static int dlugoscMaksymalnegoCiaguDodatnich(int tab[]){
        int naj_dlugosc = 0;
        int ilosc = 0;
        for(int i=0; i<tab.length; i++){
            if(tab[i]>naj_dlugosc){
                ilosc++;
            }
            else{
                ilosc = 0;
            }
            if(ilosc>naj_dlugosc){
                naj_dlugosc = ilosc;
            }
        }
        return naj_dlugosc;
    }

    public static void signum(int tab[]){
        for(int i=0; i<tab.length; i++){
            if(tab[i]>0){
                tab[i]=1;
            }
            else{
                tab[i]=-1;
            }
            System.out.print(tab[i] + ", ");
        }
        System.out.println();
    }

    public static void odwrocFragment(int tab[], int lewy, int prawy){
        int tmp;
        for(int i=0; i<(prawy - lewy) / 2; i++){
            tmp = tab[lewy+i];
            tab[lewy+i] = tab[prawy - i];
            tab[prawy - i] = tmp;
        }

        for(int i=0; i<tab.length; i++){
            System.out.print(tab[i] + ", ");
        }
        System.out.println();
    }
}