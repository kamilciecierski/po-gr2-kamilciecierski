package pl.edu.uwm.wmii.kamilciecierski.laboratorium02;
import java.util.Random;

public class Zad3 {
    public static void fun(int m, int n, int k) {
        int[][] a = new int[m][n];
        int[][] b = new int[n][k];
        int[][] c = new int[m][k];


        Random r = new Random();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = r.nextInt(10);
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < k; j++) {
                b[i][j] = r.nextInt(10);
            }
        }

        System.out.println("Macierz A");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("Macierz B");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < k; j++) {
                System.out.print(b[i][j] + " ");
            }
            System.out.println();
        }

        for(int i=0; i<m; i++){
            for(int j=0; j<k; j++){
                int suma=0;
                for(int z=0; z<n; z++){
                    suma+=a[i][z]*b[z][j];
                    c[i][j]=suma;
                }
            }
        }

        System.out.println("Macierz C");
        for (int i=0; i<m; i++){
            for (int j=0; j<k; j++){
                System.out.print(c[i][j] + " ");
            }
            System.out.println();
        }
    }
}