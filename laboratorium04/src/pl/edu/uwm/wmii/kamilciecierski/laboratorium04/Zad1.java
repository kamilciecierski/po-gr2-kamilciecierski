package pl.edu.uwm.wmii.kamilciecierski.laboratorium04;

public class Zad1 {
    public static int countChar(String str, char c){
        int n = 0;
        for(int i=0; i<str.length(); i++){
            if(str.charAt(i) == c){
                n++;
            }
        }
        return n;
    }


    public static int countSubStr(String str, String subStr){
        int lastIndex = 0;
        int count = 0;

        while(lastIndex != -1){
            lastIndex = str.indexOf(subStr,lastIndex);
            if(lastIndex != -1){
                count ++;
                lastIndex += subStr.length();
            }
        }
        return count;
    }


    public static String middle(String str){
        int len = str.length();
        if(len%2!=0){
            char a = str.charAt(len/2);
            return String.valueOf(a);
        }
        else{
            len = len/2;
            char b = str.charAt(len-1);
            char c = str.charAt(len);
            String value = String.valueOf(b)+String.valueOf(c);
            return value;
        }
    }


    public static String repeat(String str, int n){
        String powtorz = str.repeat(n);
        return powtorz;
    }


    public static int[] where(String str, String subStr) {
        int[] tmp = new int[Zad1.countSubStr(str, subStr)];
        if (tmp.length > 0) {
            String ind = str;
            tmp[0] = ind.indexOf(subStr);
            for (int i = 1; i < tmp.length; i++) {
                ind = str.substring(tmp[i - 1] + 1);
                tmp[i] = ind.indexOf(subStr) + tmp[i - 1] + 1;
            }
        }
        return tmp;
    }


    public static String change(String str){
        StringBuffer strB = new StringBuffer();
        for(int i=0; i<str.length(); i++){
            if(Character.isUpperCase(str.charAt(i))){
                strB.append(String.valueOf(str.charAt(i)).toLowerCase());
            }
            else{
                strB.append(String.valueOf(str.charAt(i)).toUpperCase());
            }
        }
        return strB.toString();
    }


    public static String nice(String str){
        StringBuffer strB = new StringBuffer("");
        int ile=0;

        for(int i=0; i<str.length(); i++){
            strB.append(str.charAt(i));
            ile++;
            if(ile%3==str.length()%3 && i!=str.length()-1){
                strB.append("'");
            }
        }
        return strB.toString();
    }


    public static String nice(String str, char znak, int n){
        StringBuffer strB = new StringBuffer("");
        int ile=0;

        for(int i=0; i<str.length(); i++){
            strB.append(str.charAt(i));
            ile++;
            if(ile%n==str.length()%n && i!=str.length()-1){
                strB.append(znak);
            }
        }
        return strB.toString();
    }
}
