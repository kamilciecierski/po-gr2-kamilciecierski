package pl.edu.uwm.wmii.kamilciecierski.laboratorium04;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Zad2{
    public static int wystapieniaZnaku(String nazwaPliku, char znak){
        int ile = 0;
        try{
            Scanner file = new Scanner(new File(nazwaPliku));

            while(file.hasNextLine()){
                String line = file.nextLine();
                for(int i = 0; i < line.length(); i++){
                    if(line.charAt(i) == znak){
                        ile++;
                    }
                }
            }
            file.close();
        }
        catch(FileNotFoundException ex){
            System.out.println("File does not exist");
        }
        return ile;
    }
}
