package pl.edu.uwm.wmii.kamilciecierski.laboratorium04;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Zad3{
    public static int wystapieniaWyrazu(String nazwaPliku, String wyraz){
        int count = 0;
        try{
            Scanner file = new Scanner(new File(nazwaPliku));

            while(file.hasNextLine()){
                String line = file.nextLine();
                int lenStr=line.length();
                int lenWyraz=wyraz.length();
                int index=0;
                String tmp;
                while(lenStr-index >= lenWyraz){
                    tmp = line.substring(index,index+(lenWyraz));
                    if(tmp.equals(wyraz)){
                        count++;
                    }
                    index++;
                }
            }
            file.close();
        }
        catch(FileNotFoundException ex){
            System.out.println("File does not exist");
        }
        return count;
    }
}
