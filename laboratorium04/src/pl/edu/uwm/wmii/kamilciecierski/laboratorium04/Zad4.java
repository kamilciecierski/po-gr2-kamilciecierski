package pl.edu.uwm.wmii.kamilciecierski.laboratorium04;
import java.math.BigInteger;

public class Zad4 {
    public static String zad4(int n){
        BigInteger podst = new BigInteger("2");
        BigInteger pot = new BigInteger(podst.pow(n*n).toString());
        BigInteger wynik = new BigInteger("1");
        return pot.subtract(wynik).toString();
    }
}
