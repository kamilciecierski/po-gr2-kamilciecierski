package pl.edu.uwm.wmii.kamilciecierski.laboratorium04;
import java.math.BigDecimal;

public class Zad5 {
    public static String zad5(float p, float k, int n){
        BigDecimal stopaProc = new BigDecimal(String.valueOf(p));
        BigDecimal kapital = new BigDecimal(String.valueOf(k));
        BigDecimal sto = new BigDecimal(100);
        return kapital.multiply(BigDecimal.ONE.add(stopaProc.divide(sto)).pow(n)).setScale(2,BigDecimal.ROUND_HALF_UP).toString();
    }
}
