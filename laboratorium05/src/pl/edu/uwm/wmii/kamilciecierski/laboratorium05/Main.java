package pl.edu.uwm.wmii.kamilciecierski.laboratorium05;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> lista1 = new ArrayList<>(Arrays.asList(1,4,9,16));
        ArrayList<Integer> lista2 = new ArrayList<>(Arrays.asList(9,7,4,9,11));

//        System.out.println(Zad1.append(lista1, lista2));
//        System.out.println(Zad2.merge(lista1, lista2));
//        System.out.println(Zad3.mergeSorted(lista1, lista2));
//        System.out.println(Zad4.reversed(lista1));
        System.out.println("Lista1 przed zamianą: " + lista1);
        Zad5.reverse(lista1);
        System.out.println("Lista1 po zamianie: " + lista1);
    }
}
