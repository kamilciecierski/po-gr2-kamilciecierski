package pl.edu.uwm.wmii.kamilciecierski.laboratorium05;
import java.util.ArrayList;
import java.util.Collections;

public class Zad3 {
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> lista = new ArrayList<>();
        Collections.sort(a);
        Collections.sort(b);

        int i=0;
        int j=0;
        for(int k=0; k<a.size()+b.size(); k++){
            if(i<a.size() && j<b.size()){
                if(a.get(i) > b.get(j)){
                    lista.add(b.get(j));
                    j++;
                }
                else{
                    lista.add(a.get(i));
                    i++;
                }
            }
            else if(i<a.size()){
                lista.add(a.get(i));
                i++;
            }
            else{
                lista.add(b.get(j));
                j++;
            }
        }
        return lista;
    }
}
