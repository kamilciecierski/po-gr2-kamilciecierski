package pl.edu.uwm.wmii.kamilciecierski.laboratorium06;

public class IntegerSet {
    private final boolean[] zbior;
    public IntegerSet(){
        this.zbior = new boolean[100];
    }

    public static IntegerSet union(IntegerSet a, IntegerSet b){
        IntegerSet n = new IntegerSet();
        for(int i=0; i<100; i++){
            if(a.zbior[i] || b.zbior[i]){
                n.zbior[i]=true;
            }
        }
        return n;
    }

    public static IntegerSet intersection(IntegerSet a, IntegerSet b){
        IntegerSet n = new IntegerSet();
        for(int i=0; i<100; i++){
            if(a.zbior[i] && b.zbior[i]){
                n.zbior[i]=true;
            }
        }
        return n;
    }

    public void insertElement(int k){
        this.zbior[k-1]=true;
    }

    public void deleteElement(int k){
        this.zbior[k-1]=false;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(int i=0; i<100; i++){
            if(this.zbior[i]){
                sb.append(i+1);
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    public boolean equals(IntegerSet n){
        return this.toString().equals(n.toString());
    }
}
