package pl.edu.uwm.wmii.kamilciecierski.laboratorium06;

public class RachunekBankowy {
    private static double rocznaStopaProcentowa;
    private double saldo;

    public RachunekBankowy(double saldo){
        this.saldo = saldo;
    }

    public void obliczMiesieczneOdsetki(){
        this.saldo += (this.saldo*rocznaStopaProcentowa)/12;
    }

    static void setRocznaStopaProcentowa(double n){
        rocznaStopaProcentowa = n;
    }

    public double getSaldo(){
        return this.saldo;
    }
}
