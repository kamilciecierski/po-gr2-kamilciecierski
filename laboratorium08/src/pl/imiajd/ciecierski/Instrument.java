package pl.imiajd.ciecierski;

import java.time.LocalDate;

abstract class Instrument {
    public Instrument(String producent, LocalDate rokProdukcji){
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }

    public String getProducent(){
        return producent;
    }

    public LocalDate getRokProdukcji(){
        return rokProdukcji;
    }

    public abstract String dzwiek();

    public boolean equals(Object obj){
        return this.toString().equals(obj.toString());
    }

    public String toString() {
        return "Producent: " + producent + ", rok produkcji: " + rokProdukcji + "\n";
    }

    private String producent;
    private LocalDate rokProdukcji;
}
