package pl.imiajd.ciecierski;

import java.time.LocalDate;

class Pracownik extends Osoba
{
    public Pracownik(String nazwisko, String[] imiona, LocalDate dataUrodzenia, Boolean plec, double pobory, LocalDate dataZatrudnienia)
    {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł, zatrudniony %s", pobory, dataZatrudnienia.toString());
    }

    public LocalDate getDataZatrudnienia(){
        return dataZatrudnienia;
    }

    private double pobory;
    private LocalDate dataZatrudnienia;
}
