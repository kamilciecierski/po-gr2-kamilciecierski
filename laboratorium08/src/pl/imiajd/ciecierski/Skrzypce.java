package pl.imiajd.ciecierski;

import java.time.LocalDate;

public class Skrzypce extends Instrument{
    public Skrzypce(String prducent, LocalDate rokProdukcji){
        super(prducent, rokProdukcji);
    }

    public String dzwiek(){
        return "skrzypu skrzyp";
    }
}
