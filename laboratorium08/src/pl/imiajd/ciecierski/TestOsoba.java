package pl.imiajd.ciecierski;
import java.time.LocalDate;
import java.util.*;

public class TestOsoba {

    public static void main(String[] args) {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik("Kowalski",new String[]{"Jan"}, LocalDate.of(1983,11,07),false, 50000, LocalDate.of(2003,05,14));
        ludzie[1] = new Student("Nowak",new String[]{"Małgorzata, Ewa"}, LocalDate.of(1998,03,17),true, "informatyka",4.5);

        for (Osoba p : ludzie) {
            for ( String i : p.getImiona()){
                System.out.print(i+" ");
            }
            System.out.print(p.getNazwisko() + ": " + p.getOpis()+ ", urodzony: "+p.getDataUrodzenia()+", plec: ");
            if(p.isPlec()){
                System.out.println("kobieta");
            }
            else {
                System.out.println("mężczyzna");
            }
        }
    }
}