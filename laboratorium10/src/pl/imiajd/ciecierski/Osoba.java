package pl.imiajd.ciecierski;

import java.time.LocalDate;

public class Osoba implements Cloneable, Comparable<Osoba>{
    public Osoba(String nazwisko, LocalDate dataUrodzenia){
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    public String getNazwisko(){
        return nazwisko;
    }

    public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }

    public String toString(){
        return this.getClass().getSimpleName() + "[" + this.nazwisko+ ", " + this.dataUrodzenia.toString() + "]";
    }


    public boolean equals(Object obj) {
        Osoba o = (Osoba) obj;
        return (o.nazwisko.equals(this.nazwisko) && o.dataUrodzenia.equals(this.dataUrodzenia));
    }


    public int compareTo(Osoba o) {
        int compareNazwisko = this.nazwisko.compareTo(o.nazwisko);
        if(compareNazwisko == 0){
            return this.dataUrodzenia.compareTo(o.dataUrodzenia);
        }
        return compareNazwisko;
    }

    private String nazwisko;
    private LocalDate dataUrodzenia;


}
