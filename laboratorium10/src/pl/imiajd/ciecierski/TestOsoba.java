package pl.imiajd.ciecierski;

import java.util.ArrayList;
import java.time.LocalDate;
import java.util.Collections;

public class TestOsoba {

    public static void main(String[] args) {
        ArrayList<Osoba> grupa = new ArrayList<>(5);
        grupa.add(new Osoba("Kowalski", LocalDate.of(1972, 6, 14)));
        grupa.add(new Osoba("Nowak", LocalDate.of(1972, 6, 14)));
        grupa.add(new Osoba("Wiśniewski", LocalDate.of(1997, 3, 1)));
        grupa.add(new Osoba("Ciecierski", LocalDate.of(1999, 8, 28)));
        grupa.add(new Osoba("Kowalski", LocalDate.of(1998, 12, 12)));


        System.out.println(grupa);
        Collections.sort(grupa);
        System.out.println(grupa);
    }
}
