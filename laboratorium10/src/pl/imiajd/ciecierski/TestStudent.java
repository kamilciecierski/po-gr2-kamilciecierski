package pl.imiajd.ciecierski;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestStudent {
    public static void main(String[] args) {
        ArrayList<Osoba> grupa = new ArrayList<>(5);
        grupa.add(new Student("Kowalski", LocalDate.of(1972, 6, 14),3.5));
        grupa.add(new Student("Nowak", LocalDate.of(1972, 6, 14),4.2));
        grupa.add(new Student("Wiśniewski", LocalDate.of(1997, 3, 1),4.9));
        grupa.add(new Student("Ciecierski", LocalDate.of(1999, 8, 28),4.3));
        grupa.add(new Student("Kowalski", LocalDate.of(1998, 12, 12),5.0));

        System.out.println(grupa);
        Collections.sort(grupa);
        System.out.println(grupa);
    }
}
