package pl.imiajd.ciecierski;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Zadanie3_Test {
    public static void main(String[] args) {
        ArrayList<String> lista = new ArrayList<>();
        try{
            Scanner file = new Scanner(new File("zad3.txt"));

            while(file.hasNextLine()){
                lista.add(file.nextLine());
                }
            file.close();
        }
        catch(FileNotFoundException ex){
            ex.printStackTrace();
        }
        System.out.println(lista);
        Collections.sort(lista);
        System.out.println(lista);
    }
}
