package pl.imiajd.ciecierski;

public class ArrayUtil<T>{

    //Zad.3
    public static <T extends Comparable<T>> void isSorted(T[] array){
        for (int index = 1; index < array.length; index++) {
            T key = array[index];
            int position = index;

            while (position > 0 && array[position - 1].compareTo(key) > 0) {
                array[position] = array[position - 1];
                position--;
            }
            array[position] = key;
        }
    }




    //Zad.4
    public static <T extends Comparable<? super T>> int binSearch(T[] array, T target) {
        int first = 0;
        int last = array.length;
        int mid;
        T midvalue;
        int origLast = last;

        while (first < last)
        {
            mid = (first+last)/2;
            midvalue = array[mid];
            if (target.compareTo(midvalue) == 0)
                return mid;
            else if (target.compareTo(midvalue) < 0)
                last = mid;
            else
                first = mid+1;
        }
        return -1;
    }




    //Zad.5
    public static <T extends Comparable<T>> void selectionSort(T[] array) {
        int min;
        T temp;

        for (int index = 0; index < array.length-1; index++) {
            min = index;
            for (int i = index+1; i < array.length; i++)
                if (array[i].compareTo(array[min])<0)
                    min = i;

            temp = array[min];
            array[min] = array[index];
            array[index] = temp;
        }
    }



    //Zad.6
    public static <T extends Comparable<T>> void mergeSort(T[] array) {
        mergeSort(array, 0, array.length - 1);
    }

    private static <T extends Comparable<T>> void mergeSort(T[] array, int min, int max) {
        if (min < max) {
            int mid = (min + max) / 2;
            mergeSort(array, min, mid);
            mergeSort(array, mid+1, max);
            merge(array, min, mid, max);
        }
    }

    private static <T extends Comparable<T>> void merge(T[] array, int first, int mid, int last) {
        T[] temp = (T[])(new Comparable[array.length]);

        int first1 = first, last1 = mid;
        int first2 = mid+1, last2 = last;
        int index = first1;

        while (first1 <= last1 && first2 <= last2) {
            if (array[first1].compareTo(array[first2]) < 0) {
                temp[index] = array[first1];
                first1++;
            }
            else {
                temp[index] = array[first2];
                first2++;
            }
            index++;
        }

        while (first1 <= last1) {
            temp[index] = array[first1];
            first1++;
            index++;
        }

        while (first2 <= last2) {
            temp[index] = array[first2];
            first2++;
            index++;
        }

        for (index = first; index <= last; index++)
            array[index] = temp[index];
    }
}
