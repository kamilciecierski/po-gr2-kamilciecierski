package pl.imiajd.ciecierski;

import java.time.LocalDate;
import java.util.Arrays;

public class ArrayUtilTEST {
    public static void main(String[] args) {
        Integer[] array = new Integer[]{4,6,-1,9,2,15,-2,7,-12};
        LocalDate[] ld = new LocalDate[]{LocalDate.of(1999,12,1),LocalDate.of(1998,8,14),LocalDate.of(1997,6,12)};

        System.out.println(Arrays.toString(array));
        ArrayUtil.isSorted(array);
        System.out.println(Arrays.toString(array));
        ArrayUtil.selectionSort(array);
        System.out.println(Arrays.toString(array));
        ArrayUtil.mergeSort(array);
        System.out.println(Arrays.toString(array));
        System.out.println(ArrayUtil.binSearch(array,6));
        System.out.println();

        System.out.println(Arrays.toString(ld));
        ArrayUtil.isSorted(ld);
        System.out.println(Arrays.toString(ld));
        ArrayUtil.selectionSort(ld);
        System.out.println(Arrays.toString(ld));
        ArrayUtil.mergeSort(ld);
        System.out.println(Arrays.toString(ld));
        System.out.println(ArrayUtil.binSearch(ld,LocalDate.of(1999,12,1)));
    }
}
