package pl.imiajd.ciecierski;

public class PairUtil<T>{
    public static <T>Pair<T> swap(Pair<T> mm){
        return new Pair<T>(mm.getSecond(), mm.getFirst());
    }
}
