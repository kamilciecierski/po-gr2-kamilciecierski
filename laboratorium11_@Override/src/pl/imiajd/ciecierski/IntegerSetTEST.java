package pl.imiajd.ciecierski;

public class IntegerSetTEST {
    public static void main(String[] args) {
        IntegerSet a = new IntegerSet();
        IntegerSet b = new IntegerSet();
        a.insertElement(3);
        a.insertElement(6);
        a.insertElement(15);
        a.insertElement(9);
        a.insertElement(3);

        b.insertElement(4);
        b.insertElement(8);
        b.insertElement(41);
        b.insertElement(58);
        b.insertElement(77);

        System.out.println(a);
        System.out.println(b);

        System.out.println(IntegerSet.union(a,b));
        System.out.println(IntegerSet.intersection(a,b));

        a.deleteElement(2);
        a.deleteElement(31);

        System.out.println(a.toString());
        System.out.println(b.toString());

        System.out.println(a.equals(b));
    }
}
