package pl.imiajd.ciecierski;

public class Nauczyciel extends Osoba {
    private double pensja;

    public Nauczyciel(String nazwisko, int rokUrodzenia, String kierunek, double pensja){
        super(nazwisko,rokUrodzenia);
        this.pensja = pensja;
    }

    public double getPensja(){
        return this.pensja;
    }
    @Override
    public String toString(){
        return "Nauczyciel:\n"+super.toString()+"\nPensja: "+this.pensja;
    }
}
