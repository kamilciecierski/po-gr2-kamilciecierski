package pl.imiajd.ciecierski;

import java.time.LocalDate;

public class Osoba3 implements Cloneable, Comparable<Osoba3>{
    public Osoba3(String nazwisko, LocalDate dataUrodzenia){
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    public String getNazwisko(){
        return nazwisko;
    }

    public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }

    @Override
    public String toString(){
        return this.getClass().getSimpleName() + "[" + this.nazwisko+ ", " + this.dataUrodzenia.toString() + "]";
    }

    @Override
    public boolean equals(Object obj) {
        Osoba3 o = (Osoba3) obj;
        return (o.nazwisko.equals(this.nazwisko) && o.dataUrodzenia.equals(this.dataUrodzenia));
    }

    @Override
    public int compareTo(Osoba3 o) {
        int compareNazwisko = this.nazwisko.compareTo(o.nazwisko);
        if(compareNazwisko == 0){
            return this.dataUrodzenia.compareTo(o.dataUrodzenia);
        }
        return compareNazwisko;
    }

    private String nazwisko;
    private LocalDate dataUrodzenia;
}
