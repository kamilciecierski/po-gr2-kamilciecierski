package pl.imiajd.ciecierski;

import java.time.LocalDate;

class Student2 extends Osoba2
{
    public Student2(String nazwisko, String[] imiona, LocalDate dataUrodzenia, Boolean plec, String kierunek, double sredniaOcen)
    {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    @Override
    public String getOpis()
    {
        return "kierunek studiów: " + kierunek + ", średnia ocen: " + sredniaOcen;
    }

    public double getSredniaOcen(){
        return sredniaOcen;
    }

    public void setSredniaOcen(double sredniaOcen){
        this.sredniaOcen = sredniaOcen;
    }

    private String kierunek;
    private double sredniaOcen;
}
