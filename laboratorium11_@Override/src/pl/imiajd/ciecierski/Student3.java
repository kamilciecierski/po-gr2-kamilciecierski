package pl.imiajd.ciecierski;

import java.time.LocalDate;

public class Student3 extends Osoba3 implements Cloneable, Comparable<Osoba3>{
    public Student3(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen){
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }

    @Override
    public String toString(){
        return this.getClass().getSimpleName() + "[" + this.getNazwisko()+ ", " +
                this.getDataUrodzenia().toString() + ", " + this.sredniaOcen + "]";
    }

    @Override
    public int compareTo(Osoba3 o) {
        int last = super.compareTo((o));
        if((o instanceof Student3)&&(last==0)){
            return -(int)Math.ceil(this.sredniaOcen-((Student3) o).sredniaOcen);
        }
        return last;
    }

    private double sredniaOcen;
}
