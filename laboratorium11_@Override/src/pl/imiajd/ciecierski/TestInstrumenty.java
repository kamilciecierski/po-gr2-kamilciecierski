package pl.imiajd.ciecierski;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestInstrumenty {
    public static void main(String[] args) {
        ArrayList<Instrument> orkiestra = new ArrayList<>();
        orkiestra.add(new Skrzypce("Yamaha", LocalDate.of(2018,6,21)));
        orkiestra.add(new Fortepian("Petrof", LocalDate.of(2010,7,17)));
        orkiestra.add(new Skrzypce("Wittner", LocalDate.of(2017,8,27)));
        orkiestra.add(new Flet("Jupiter", LocalDate.of(2019,6,5)));
        orkiestra.add(new Flet("Jupiter", LocalDate.of(2020,2,7)));

        for(Instrument inst: orkiestra){
            System.out.print(inst.dzwiek() + " ");
            System.out.println(inst);
        }
        System.out.println(orkiestra);

        System.out.println(orkiestra.get(0).equals(orkiestra.get(3)));
        System.out.println(orkiestra.get(1).equals(orkiestra.get(2)));
        System.out.println(orkiestra.get(2).getProducent());
        System.out.println(orkiestra.get(2).getRokProdukcji());
    }
}
