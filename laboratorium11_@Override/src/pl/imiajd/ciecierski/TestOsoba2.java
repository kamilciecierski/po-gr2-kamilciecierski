package pl.imiajd.ciecierski;

import java.time.LocalDate;

public class TestOsoba2 {

    public static void main(String[] args) {
        Osoba2[] ludzie = new Osoba2[2];

        ludzie[0] = new Pracownik2("Kowalski",new String[]{"Jan"}, LocalDate.of(1983,11,07),false, 50000, LocalDate.of(2003,05,14));
        ludzie[1] = new Student2("Nowak",new String[]{"Małgorzata, Ewa"}, LocalDate.of(1998,03,17),true, "informatyka",4.5);

        for (Osoba2 p : ludzie) {
            for ( String i : p.getImiona()){
                System.out.print(i+" ");
            }
            System.out.print(p.getNazwisko() + ": " + p.getOpis()+ ", urodzony: "+p.getDataUrodzenia()+", plec: ");
            if(p.isPlec()){
                System.out.println("kobieta");
            }
            else {
                System.out.println("mężczyzna");
            }
        }
    }
}