package pl.imiajd.ciecierski;

import java.util.Scanner;
import java.util.Stack;

public class Cyfry {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj liczbę: ");
        int n;

        while (true) {
            n = scanner.nextInt();
            if (n >= 0)
                break;
            else
                System.out.println("Liczba musi być nieujemna");

        }
        Cyfry.cyfry(n);
    }

    public static void cyfry(int n) {
        if (n == 0) {
            System.out.println("0");
        } else {
            Stack<Integer> st = new Stack<>();
            while (n != 0) {
                st.push(n % 10);
                n /= 10;
            }
            while (!st.empty()) {
                System.out.print(st.pop() + " ");
            }
            System.out.println();
        }
    }
}

