package pl.imiajd.ciecierski;

import java.util.LinkedList;

public class Odwroc {
    public static void main(String[] args) {
        LinkedList<String> pracownicy = new LinkedList<>();
        pracownicy.add("Jan Kowalski");
        pracownicy.add("Piotr Nowak");
        pracownicy.add("Andrzej Wiśniewski");
        pracownicy.add("Julia Kowalewska");
        pracownicy.add("Beata Wawrzyniak");

        System.out.println(pracownicy);
        odwroc(pracownicy);
        System.out.println(pracownicy);


        LinkedList<String> listB = new LinkedList<>();
        listB.add("2");
        listB.add("4");
        listB.add("-15");
        listB.add("24");
        listB.add("8");

        System.out.println(listB);
        odwroc(listB);
        System.out.println(listB);
    }

    //Zad.3
//    public static void odwroc(LinkedList<String> lista){
//        LinkedList<String> newList = new LinkedList<>(lista);
//        lista.clear();
//        for(int i=newList.size()-1; i>=0; i--){
//            lista.add(newList.get(i));
//        }
//    }

    public static <T extends Comparable<T>> void odwroc(LinkedList<T> lista){
        LinkedList<T> newList = new LinkedList<>(lista);
        lista.clear();
        for(int i=newList.size()-1; i>=0; i--){
            lista.add(newList.get(i));
        }
    }
}
