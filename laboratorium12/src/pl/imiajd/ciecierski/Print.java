package pl.imiajd.ciecierski;

import java.util.Iterator;
import java.util.LinkedList;

public class Print {
    public static void main(String[] args) {
        LinkedList<Integer> liczby = new LinkedList<>();
        liczby.add(3);
        liczby.add(5);
        liczby.add(-4);
        liczby.add(8);
        liczby.add(-9);


        Print.print(liczby);
    }
    public static <T extends Iterable<?>> void print(T object){
        Iterator<?> it = object.iterator();
        while (it.hasNext()){
            System.out.print(it.next());
            if (it.hasNext()){
                System.out.print(", ");
            }
        }
        System.out.println();
    }
}
