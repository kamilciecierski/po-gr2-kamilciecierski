package pl.imiajd.ciecierski;

import java.util.LinkedList;

public class Redukuj {

    public static void main(String[] args) {
        LinkedList<String> pracownicy = new LinkedList<>();
        pracownicy.add("Jan Kowalski");
        pracownicy.add("Piotr Nowak");
        pracownicy.add("Andrzej Wiśniewski");
        pracownicy.add("Julia Kowalewska");
        pracownicy.add("Beata Wawrzyniak");
        pracownicy.add("Jan Kowalski");
        pracownicy.add("Piotr Nowak");
        pracownicy.add("Andrzej Wiśniewski");
        pracownicy.add("Julia Kowalewska");
        pracownicy.add("Beata Wawrzyniak");

        System.out.println(pracownicy);
        redukuj(pracownicy,3);
        System.out.println(pracownicy);
    }

    //Zad.1
//    public static void redukuj(LinkedList<String> pracownicy, int n) {
//        for (int i = n-1; i < pracownicy.size(); i += n-1) {
//            pracownicy.remove(i);
//        }
//    }

    public static <T> void redukuj(LinkedList<T> pracownicy, int n) {
        for (int i = n-1; i < pracownicy.size(); i += n-1) {
            pracownicy.remove(i);
        }
    }
}
