package pl.imiajd.ciecierski;

import java.util.BitSet;

public class SitoEratostenesa {
    public static void main(String[] args) {
        SitoEratostenesa.sitoEratostenesa(100);
    }

    public static void sitoEratostenesa(int n){
        BitSet bs = new BitSet(n + 1);
        for (int j = 2; j <= n; ++j) {
            bs.set(j);
        }
        int j = 2;
        while (j * j <= n) {
            if (bs.get(j)) {
                int k = 2 * j;
                while (k <= n) {
                    bs.clear(k);
                    k += j;
                }
            }
            ++j;
        }
        int[] primes= bs.stream().toArray();
        for(int x: primes){
            System.out.print(x + " ");
        }
        System.out.println();
    }
}
