package pl.imiajd.ciecierski;
import java.util.Stack;

public class StackTEST {
    public static void main(String[] args) {
        String sentence = "Ala ma kota.";
        System.out.println(sentence);
        System.out.println(odwroc(sentence));
    }

    public static String odwroc(String str){
        String[] words = str.split(" ");
        Stack<String> st = new Stack<>();
        StringBuilder reversed = new StringBuilder();
        for(String word : words){
            st.push(word);
            if(word.endsWith(".")){
                while(!st.empty()){
                    StringBuilder wordReversed = new StringBuilder();
                    wordReversed.append(st.pop());
                    if(st.empty()){
                        wordReversed.setCharAt(0,Character.toLowerCase(wordReversed.charAt(0)));
                        reversed.append(wordReversed);
                        reversed.append(". ");
                    }
                    else if(wordReversed.toString().equals(word)){
                        wordReversed.setCharAt(0,Character.toUpperCase(wordReversed.charAt(0)));
                        reversed.append(wordReversed, 0, wordReversed.length()-1);
                        reversed.append(" ");
                    }
                    else{
                        reversed.append(wordReversed);
                        reversed.append(" ");
                    }
                }
            }
        }
        return reversed.toString();
    }
}
