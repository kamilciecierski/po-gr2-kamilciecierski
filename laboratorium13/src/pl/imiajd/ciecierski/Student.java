package pl.imiajd.ciecierski;

class Student implements Comparable<Student>{
    String imie;
    String nazwisko;
    int id;
    public Student(String imie,String nazwisko,int id){
        this.id = id;
        this.imie = imie;
        this.nazwisko=nazwisko;
    }

    public int getId(){

        return this.id;
    }

    @Override
    public String toString() {

        return "Nazwisko: "+ this.nazwisko + " Imie: "+ this.imie + " ID: "+ this.id;
    }

    @Override
    public int compareTo(Student o) {
        int result = this.nazwisko.compareTo(o.nazwisko);
        if(result==0){
            result=this.imie.compareTo(o.imie);
        }
        if(result==0){
            result = Integer.compare(this.id,o.id);
        }
        return result;
    }
}
