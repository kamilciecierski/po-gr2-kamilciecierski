package pl.imiajd.ciecierski;

import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeMap;

public class StudentTEST {
    public static void sortedArray(HashMap<Student,String> x){
        TreeMap<Student, String> sort = new TreeMap<>(x);
        sort.forEach((key, value) -> System.out.println(key + " Ocena: " + value));
    }
    public static void main(String[] args) {
        HashMap<Student,String> studenci = new HashMap<>();
        Student s1 = new Student("Jan","Kowalski",2133);
        Student s2 = new Student("Tateusz","Kwiatkowski",2077);
        Student s3= new Student("Zofia","Wiśniewska",2121);
        Student s4= new Student("Marian","Nowak",1111);

        studenci.put(s1,"db");
        studenci.put(s2,"bdb");
        studenci.put(s3,"db+");
        studenci.put(s4,"dst");


        System.out.println("[dodaj] - dodaje studenta\n" +
                "[usun] - usuwa studenta\n" +
                "[aktualizuj] - zmienia ocenę studenta\n" +
                "[wypisz] - wypisuje listę studentów\n" +
                "[zakoncz] - program kończy pracę");

        while (true){
            Scanner scan = new Scanner(System.in);
            String n = scan.nextLine();
            HashMap<Student,String> pomoc = new HashMap<>();
            if (n.equals("dodaj")){
                System.out.println("Podaj imię, nazwisko, ID i ocenę (np. Jan Kowalski 3421 db");
                String y= scan.nextLine();
                String [] splitted = y.split("\\s+");
                studenci.put(new Student(splitted[0],splitted[1],Integer.parseInt(splitted[2])),splitted[3]);
                pomoc.put(new Student(splitted[0],splitted[1],Integer.parseInt(splitted[2])),splitted[3]);
            }
            if(n.equals("usun")){
                System.out.println("Podaj ID studenta, którego chcesz usunąć");
                String u = scan.nextLine();
                int identyfikator = Integer.parseInt(u);
                Student s = null;
                for(Student k : studenci.keySet()){
                    if(k.getId() == identyfikator){
                        s=k;
                        break;
                    }
                }
                if(s != null){
                    studenci.remove(s);
                }
            }
            if (n.equals("aktualizuj")){
                System.out.println("Podaj ID studenta i nową ocenę (np. 2133 dst)");
                String a = scan.nextLine();
                String [] splitted = a.split("\\s+");
                int identyfikator = Integer.parseInt(splitted[0]);
                Student s = null;
                for(Student k : studenci.keySet()){
                    if(k.getId() == identyfikator){
                        s=k;
                        break;
                    }

                }
                studenci.replace(s,splitted[1]);
            }
            if(n.equals("wypisz")){
                sortedArray(studenci);
            }
            if (n.equals("zakoncz")){
                break;
            }
        }
    }
}
