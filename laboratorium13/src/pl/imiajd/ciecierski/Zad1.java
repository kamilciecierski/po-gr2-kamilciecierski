package pl.imiajd.ciecierski;

public class Zad1 implements Comparable<Zad1>{
    public Zad1(String [] zadanie, int priorytet){
        this.priorytet = priorytet;
        this.opis = zadanie;
    }

    public int getPriorytet(){
        return priorytet;
    }

    @Override
    public String toString(){
        System.out.println("Zadanie: ");
        for(String i : opis){
            System.out.print(i + " ");
        }
        return " ";
    }

    @Override
    public int compareTo(Zad1 o) {
        if (this.getPriorytet() > o.getPriorytet()){
            return 1;
        }
        else if (this.getPriorytet() < o.getPriorytet()){
            return -1;
        }
        else{
            return 0;
        }
    }

    int priorytet;
    String [] opis;
}
