package pl.imiajd.ciecierski;

import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Scanner;

public class Zad1TEST {
    public static void main(String[] args) {
        PriorityQueue<Zad1> listaZadan = new PriorityQueue<>();

        System.out.println("[dodaj priorytet opis] - dodaje zadanie (np. dodaj 2 zakupy)\n" +
                "[nastepne] - usuwa najbardziej pilne zadanie\n" +
                "[zakoncz] - program kończy pracę");

        while (true){
            Scanner scan = new Scanner(System.in);
            String n = scan.nextLine();
            String [] tab = n.split("\\s+");
            if (tab[0].equals("dodaj")){
                listaZadan.add(new Zad1(Arrays.copyOfRange(tab,2, tab.length), Integer.parseInt(tab[1])));
            }
            if(tab[0].equals("nastepne")){
                listaZadan.remove();
            }
            if (tab[0].equals("zakoncz")){
                if(listaZadan.isEmpty()){
                    System.out.println("Lista zadań jest pusta");
                }
                break;
            }
        }
        while (!listaZadan.isEmpty()) {
            System.out.println(listaZadan.remove());
        }
    }
}