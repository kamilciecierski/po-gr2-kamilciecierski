package pl.imiajd.ciecierski;

import java.util.*;

public class Zad2 {
    public static void main(String[] args) {
        HashMap<String, String> studenci = new HashMap<>();
        studenci.put("Susan","bdb");
        studenci.put("Carl","db+");
        studenci.put("Joe","db");

        System.out.println("[dodaj] - dodaje studenta\n" +
                "[usun] - usuwa studenta\n" +
                "[aktualizuj] - zmienia ocenę studenta\n" +
                "[wypisz] - wypisuje listę studentów\n" +
                "[zakoncz] - program kończy pracę");

        while (true){
            Scanner scan = new Scanner(System.in);
            String n = scan.nextLine();
            if (n.equals("dodaj")){
                System.out.println("Podaj nazwisko i ocenę (np. Jonh db)");
                String d = scan.nextLine();
                String [] tab = d.split("\\s+");
                studenci.put(tab[0], tab[1]);
            }
            if(n.equals("usun")){
                System.out.println("Podaj nazwisko studenta, którego chcesz usunąć");
                String u = scan.nextLine();
                studenci.remove(u);
            }
            if (n.equals("aktualizuj")){
                System.out.println("Podaj nazwisko studenta i nową ocenę (np. Joe dst)");
                String a = scan.nextLine();
                String [] tab = a.split("\\s+");
                studenci.replace(tab[0], tab[1]);
            }
            if(n.equals("wypisz")){
                Map<String, String> map = new TreeMap<String, String>(studenci);
                Set set2 = map.entrySet();
                Iterator iterator2 = set2.iterator();
                while(iterator2.hasNext()) {
                    Map.Entry me2 = (Map.Entry) iterator2.next();
                    System.out.print(me2.getKey() + ": ");
                    System.out.println(me2.getValue());
                }
            }
            if (n.equals("zakoncz")){
                break;
            }
        }
    }
}
