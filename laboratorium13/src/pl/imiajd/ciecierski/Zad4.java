package pl.imiajd.ciecierski;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Zad4 {
    public static void main(String[] args) throws FileNotFoundException {
        Map<Integer, HashSet<String>> duplicats = new HashMap<>();
        Scanner scan = new Scanner(new File("zad4.txt"));

        while (scan.hasNext()) {
            String word = scan.nextLine();
            int h = word.hashCode();

            if (!duplicats.containsKey(h)) {
                HashSet<String> newSet = new HashSet<>();
                newSet.add(word);
                duplicats.put(h, newSet);
            } else {
                duplicats.get(h).add(word);
            }
            Set<Integer> keySet = duplicats.keySet();
            for (Integer key : keySet) {
                HashSet<String> value = duplicats.get(key);
                if (value.size() > 1) {
                    System.out.println(key + " : " + value.toString());
                }
            }
        }
    }
}
