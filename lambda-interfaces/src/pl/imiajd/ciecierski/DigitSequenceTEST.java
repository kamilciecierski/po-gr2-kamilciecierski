package pl.imiajd.ciecierski;

import java.util.Iterator;

public class DigitSequenceTEST {
    public static void main(String[] args) {
        Iterator<Integer> test = new DigitSequence(12345678);
        test.forEachRemaining( (in) -> System.out.println(in) );

        try {
            test.remove();
        }
        catch (UnsupportedOperationException e) {
            e.printStackTrace();
        }
    }
}
