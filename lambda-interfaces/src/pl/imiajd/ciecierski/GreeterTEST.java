package pl.imiajd.ciecierski;

public class GreeterTEST {
    public static void main(String[] args) {
        Greeter r1 = new Greeter(3,"Jakub");
        Greeter r2 = new Greeter(8,"Michał");
        Greeter.runTogether(r1,r2);
        Greeter.runInOrder(r1,r2);
    }

    public static class Greeter implements Runnable {

        private final int counter;
        private final String greeterText;
        Greeter(int n, String target) {
            counter = n;
            greeterText = target;
        }

        @Override
        public void run() {
            for(int i =0; i < counter; i++) {
                System.out.println("Witaj, " + greeterText);
            }
        }

        public static void runTogether(Runnable... tasks) {
            for(Runnable task : tasks) {
                new Thread(task).start();
            }
        }

        public static void runInOrder(Runnable... tasks) {
            for(Runnable task : tasks) {
                task.run();
            }
        }
    }
}
