package pl.imiajd.ciecierski;


public class IntSequenceTEST {
    public static void main(String[] args) {
        IntSequence seq = IntSequence.of(9, 8, 7, 6, 5);
        while (seq.hasNext()) System.out.println(seq.next());

        IntSequence constantOne = IntSequence.constant(1);
        for (int i = 0; i < 10; i++) {
            assert constantOne.next() == 1;
        }
    }
}
