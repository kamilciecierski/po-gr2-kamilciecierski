package pl.imiajd.ciecierski;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class LuckySortTEST {
    static void luckySort(ArrayList<String> strings, Comparator<String> comp) {
        ArrayList<String> idealArray = new ArrayList<>(strings);

        idealArray.sort(comp);
        int count = 0;
        boolean condition = (idealArray.equals(strings));
        while(!condition) {
            Collections.shuffle(strings);
            condition = (idealArray.equals(strings));
            count++;
        }

        System.out.println("Przetasowań: " + count) ;

    }

    public static void main(String[] args){
        ArrayList<String> strings = new ArrayList<>();
        strings.add("Kuba");
        strings.add("Wojtek");
        strings.add("Andrzej");
        strings.add("Michał");
        strings.add("Adam");

        luckySort(strings, (first, second) -> {
            int difference  = first.length() - second.length();
            return Integer.compare(difference, 0);
        });
    }
}
