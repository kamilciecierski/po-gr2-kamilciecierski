package pl.imiajd.ciecierski;

public interface Measurable {
    double getMeasurable();
}
