package pl.imiajd.ciecierski;

import java.io.File;
import java.util.Arrays;

public class Zad10 {
        public static void main(String[] args) {
            File f = new File(".");
            Arrays.stream(f.listFiles(File::isDirectory)).forEach(System.out::println);
        }
}
