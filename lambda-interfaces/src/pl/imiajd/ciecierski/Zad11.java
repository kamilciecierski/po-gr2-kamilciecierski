package pl.imiajd.ciecierski;

import java.io.File;
import java.util.Arrays;

public class Zad11 {
    public static void main(String[] args) {
        printFilesWithExtension(new File(".idea"), "xml");
    }

    static void printFilesWithExtension(File directory, String ext) {
        Arrays.stream(listFilesWithExtension(directory, ext)).forEach(System.out::println);
    }

    static File[] listFilesWithExtension(File directory, String ext) {
        return directory.listFiles((dir, name) -> name.endsWith(ext));
    }
}
