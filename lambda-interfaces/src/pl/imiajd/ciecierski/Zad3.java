package pl.imiajd.ciecierski;

public class Zad3 {
    public static void main(String[] args) {
        System.out.println("Typy nadrzędne dla String: Serializable, Comparable<String>, CharSequence");
        System.out.println("Typy nadrzędne dla Scanner: Closeable, AutoCloseable, Iterator<String>");
        System.out.println("Typy nadrzędne dla ImageOutputStream: AutoCloseable, Closeable, DataInput, DataOutput, ImageInputStream");
    }
}
