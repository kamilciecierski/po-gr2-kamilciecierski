package pl.imiajd.ciecierski;

public class Adres {
    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String miasto;
    private String kod_pocztowy;

    public Adres(String ulica, int numer_domu, int numer_mieszkania, String miasto, String kod_pocztowy){
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.numer_mieszkania=numer_mieszkania;
        this.miasto=miasto;
        this.kod_pocztowy=kod_pocztowy;
    }

    public Adres(String ulica, int numer_domu, String miasto, String kod_pocztowy){
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.miasto=miasto;
        this.kod_pocztowy=kod_pocztowy;
    }

    public void pokaz(){
        System.out.println(this.kod_pocztowy+" "+this.miasto);
        if(this.numer_mieszkania != 0){
            System.out.println(this.ulica+" "+this.numer_domu+"/"+this.numer_mieszkania);
        }
        else{
            System.out.println(this.ulica+" "+this.numer_domu);
        }
    }

    public String getKod_pocztowy() {
        return kod_pocztowy;
    }

    public boolean przed(String kod_pocztowy){
        for(int i=0; i<this.kod_pocztowy.length();i++){
            if(this.kod_pocztowy.charAt(i) < kod_pocztowy.charAt(i)){
                return true;
            }
            if(this.kod_pocztowy.charAt(i) > kod_pocztowy.charAt(i)){
                return false;
            }
        }
        return false;
    }
}
