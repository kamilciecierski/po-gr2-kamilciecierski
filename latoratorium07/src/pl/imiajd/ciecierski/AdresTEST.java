package pl.imiajd.ciecierski;

public class AdresTEST {
    public static void main(String[] args) {
        Adres adres1 = new Adres("Mleczna",5, 3,"Olsztyn","10-710");
        Adres adres2 = new Adres("Daszyńskiego",10,"Kraków","31-045");
        Adres adres3 = new Adres("Poselska",10,"Rzeszów","35-001");

        adres1.pokaz();
        adres2.pokaz();
        adres3.pokaz();

        System.out.println("Kod pocztowy: "+adres2.getKod_pocztowy()+" jest przed "+adres1.getKod_pocztowy()+": "+adres2.przed(adres1.getKod_pocztowy()));
        System.out.println("Kod pocztowy: "+adres1.getKod_pocztowy()+" jest przed "+adres2.getKod_pocztowy()+": "+adres1.przed(adres2.getKod_pocztowy()));
        System.out.println("Kod pocztowy: "+adres3.getKod_pocztowy()+" jest przed "+adres1.getKod_pocztowy()+": "+adres3.przed(adres1.getKod_pocztowy()));
    }
}
