package pl.imiajd.ciecierski;

import java.awt.*;

public class BetterRectangle extends Rectangle{
    public BetterRectangle(int x, int y, int w, int h){
        setLocation(x,y);
        setSize(w,h);
    }

//    public BetterRectangle(int x, int y, int w, int h){
//        super(x,y,w,h);
//    }


    public double getPerimeter(){
        return 2*(this.height+this.width);
    }

    public double getArea(){
        return this.width*this.height;
    }
}
