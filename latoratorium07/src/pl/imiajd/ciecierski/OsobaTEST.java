package pl.imiajd.ciecierski;

public class OsobaTEST {
    public static void main(String[] args) {
        Osoba Jan = new Osoba("Kowalski",1987);
        Student Adam = new Student("Nowak",1998,"Ekonomia");
        Nauczyciel Andrzej = new Nauczyciel("Pietrzak",1980,"Budownictwo",2273.5);
        System.out.println(Jan);
        System.out.println();
        System.out.println(Adam);
        System.out.println();
        System.out.println(Andrzej);
        System.out.println();
        System.out.println(Adam.getKierunek());
        System.out.println(Andrzej.getPensja());
        System.out.println(Andrzej.getNazwisko());
    }
}
